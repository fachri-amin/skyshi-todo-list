import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useNavigate, useParams } from "react-router";
import Button from "../components/Button";
import MainLayout from "../components/MainLayout";
import EmptyState from "../components/EmptyState";
import ItemList from "../components/ItemList";
import IconBack from "../assets/img/todo-back-button.png";
import SortDropdown from "../components/SortDropdown";
import EditableTitle from "../components/EditableTitle";
import {
  useTodo,
  useAddTodo,
  useEditTodo,
  useDeleteTodo,
} from "../hooks/itemList";
import { useDetailActivity, useEditActivity } from "../hooks/activity";
import { useQueryClient } from "react-query";
import ModalAddItem from "../components/ModalAddItem";
import ModalConfirmation from "../components/ModalConfirmation";
import Snackbar from "../components/Snackbar";

const badgeColor = {
  "very-high": "#ED4C5C",
  high: "#F8A541",
  normal: "#00A790",
  low: "#428BC1",
  "very-low": "#8942C1",
};

const Item = () => {
  const queryClient = useQueryClient();
  const { id } = useParams();
  const { data } = useTodo(id);
  const { data: detailActivity } = useDetailActivity(id);
  const navigate = useNavigate();
  const { mutate } = useAddTodo();
  const { mutate: mutateEditActivity } = useEditActivity(id);
  const { mutate: mutateEditTodo } = useEditTodo();
  const { mutate: mutateDeleteTodo } = useDeleteTodo();
  const [showSnackbar, setShowSnackbar] = React.useState(false);
  const [showModal, setShowModal] = React.useState(false);
  const [showModalConfirmation, setShowModalConfirmation] =
    React.useState(false);
  const [selectedDeleteData, setSelectedDeleteData] = React.useState(null);

  const handleAdd = (data) => {
    mutate(
      {
        activity_group_id: id,
        title: data.title,
        priority: data.priority,
        is_active: 0,
      },
      {
        onSuccess: (res) => {
          queryClient.invalidateQueries("todo");
          queryClient.removeQueries("todo");
          setShowModal(false);
        },
      }
    );
  };

  const editActivity = (data) => {
    mutateEditActivity(data, {
      onSuccess: (res) => {
        queryClient.invalidateQueries("activity");
        queryClient.removeQueries("activity");
      },
    });
  };

  const checkTodo = (data) => {
    mutateEditTodo(data, {
      onSuccess: (res) => {},
    });
  };

  const deleteTodo = (data) => {
    mutateDeleteTodo(data, {
      onSuccess: (res) => {
        setShowSnackbar(true);
        setShowModalConfirmation(false);
      },
    });
  };

  React.useEffect(() => {
    if (showSnackbar) {
      setTimeout(() => {
        setShowSnackbar(false);
      }, 3000);
    }
  }, [showSnackbar]);

  return (
    <MainLayout data-cy="activity-item">
      <ModalAddItem
        activityGrupId={id}
        show={showModal}
        handleFormSubmit={(data) => handleAdd(data)}
      />
      <ModalConfirmation
        show={showModalConfirmation}
        onReject={() => setShowModalConfirmation(false)}
        onApprove={() => deleteTodo(selectedDeleteData?.id)}
        type="List Item"
        subject={selectedDeleteData?.title}
      />
      <Snackbar show={showSnackbar} type="List Item" />
      <Row className="justify-content-space-between mb-5">
        <Col>
          <div className="item-page-title-container">
            <img
              onClick={() => navigate(-1)}
              className="me-2 cursor-pointer"
              src={IconBack}
              alt=""
            />
            <EditableTitle
              text={detailActivity?.title}
              onBlur={(data) => editActivity(data)}
            />
          </div>
        </Col>
        <Col lg={3}>
          <div className="item-page-title-container">
            <SortDropdown data-cy="todo-sort-button" />
            <Button text="Tambah" onClick={() => setShowModal(true)} />
          </div>
        </Col>
      </Row>
      {!data?.data?.length && <EmptyState type="todo" />}
      <Row>
        {data?.data?.length > 0 &&
          data.data.map((item) => (
            <Col lg={12}>
              <ItemList
                title={item.title}
                checked={item.is_active}
                badgeColor={badgeColor[item.priority]}
                onCheck={() =>
                  checkTodo({ id: item.id, is_active: item.is_active ? 0 : 1 })
                }
                onClickDelete={() => {
                  setSelectedDeleteData(item);
                  setShowModalConfirmation(true);
                }}
              />
            </Col>
          ))}
      </Row>
    </MainLayout>
  );
};

export default Item;
