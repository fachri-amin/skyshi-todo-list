/* eslint-disable no-mixed-operators */
import React from "react";
import dayjs from "dayjs";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useNavigate, useLocation } from "react-router";
import Button from "../components/Button";
import MainLayout from "../components/MainLayout";
import EmptyState from "../components/EmptyState";
import ActivityCard from "../components/ActivityCard";
import {
  useActivity,
  useAddActivity,
  useDeleteActivity,
} from "../hooks/activity";
import { useQueryClient } from "react-query";
import ModalConfirmation from "../components/ModalConfirmation";
import Snackbar from "../components/Snackbar";

const Activity = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  const location = useLocation();
  const email = new URLSearchParams(location.search).get("email");
  const { data } = useActivity(email);
  const { mutate } = useAddActivity();
  const { mutate: mutateDeleteActivity } = useDeleteActivity();
  const [showSnackbar, setShowSnackbar] = React.useState(false);
  const [selectedDeleteData, setSelectedDeleteData] = React.useState(null);
  const [showModalConfirmation, setShowModalConfirmation] =
    React.useState(false);

  const handleAdd = () => {
    mutate(
      { email, title: "New Activity" },
      {
        onSuccess: (res) => {
          queryClient.invalidateQueries("activity");
          queryClient.removeQueries("activity");
        },
      }
    );
  };

  const deleteActivity = (data) => {
    mutateDeleteActivity(data, {
      onSuccess: (res) => {
        setShowSnackbar(true);
        setShowModalConfirmation(false);
      },
    });
  };

  React.useEffect(() => {
    if (showSnackbar) {
      setTimeout(() => {
        setShowSnackbar(false);
      }, 3000);
    }
  }, [showSnackbar]);

  return (
    <MainLayout data-cy="activity-item">
      <ModalConfirmation
        show={showModalConfirmation}
        onReject={() => setShowModalConfirmation(false)}
        onApprove={() => deleteActivity(selectedDeleteData?.id)}
        type="Activity"
        subject={selectedDeleteData?.title}
      />
      <Snackbar show={showSnackbar} type="Activity" />
      <Row className="justify-content-space-between mb-5">
        <Col>
          <p className="section-title">Activity</p>
        </Col>
        <Col lg={1}>
          <Button type={"primary-add"} text="Tambah" onClick={handleAdd} />
        </Col>
      </Row>
      {!data?.data?.length && <EmptyState />}
      <Row>
        {data?.data?.length > 0 &&
          data.data.map((item) => (
            <Col lg={3}>
              <ActivityCard
                onClick={() => navigate(`/item-list/${item.id}`)}
                title={item.title}
                date={dayjs(item.created_at).format("DD MMMM YYYY")}
                onClickDelete={() => {
                  setSelectedDeleteData(item);
                  setShowModalConfirmation(true);
                }}
              />
            </Col>
          ))}
      </Row>
    </MainLayout>
  );
};

export default Activity;
