import axios from "axios";
import CONFIG from "../config/env";

const BASE_URL = CONFIG.endpoint.API_ENDPOINT;
let instance = null;

function makeInstance() {
  instance = axios.create({
    baseURL: BASE_URL,
    timeout: 60000,
  });

  return instance;
}

const axiosInstance = makeInstance();

export default axiosInstance;
