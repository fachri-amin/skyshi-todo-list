import { Routes, Route } from "react-router-dom";
import Activity from "../pages/Activity";
import Item from "../pages/Item";

const Routers = () => {
  return (
    <Routes>
      <Route key="activity" path="/" exact={true} element={<Activity />} />
      <Route key="item" path="/item-list/:id" exact={true} element={<Item />} />
    </Routes>
  );
};

export default Routers;
