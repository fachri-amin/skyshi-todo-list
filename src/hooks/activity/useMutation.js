import axios from "../../services/axios";
import { useMutation, useQueryClient } from "react-query";

export function useAddActivity(options = {}) {
  return useMutation((values) =>
    axios.post("/activity-groups", values).then((res) => res.data)
  );
}
export function useEditActivity(id, options = {}) {
  const queryClient = useQueryClient();

  return useMutation(
    (formData) =>
      axios.patch(`/activity-groups/${id}`, formData).then((res) => res.data),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("activity");
        queryClient.removeQueries("activity");
      },
    }
  );
}

export function useDeleteActivity(options = {}) {
  const queryClient = useQueryClient();

  return useMutation(
    (id) => axios.delete(`/activity-groups/${id}`).then((res) => res.data),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("activity");
        queryClient.removeQueries("activity");
      },
    }
  );
}
