import { useState } from "react";
import { useQuery } from "react-query";
import axios from "../../services/axios";

async function getActivities(id, filter) {
  const { data } = await axios.get(`/activity-groups`, {
    params: { ...filter },
  });

  return data;
}

export function useActivity(email) {
  const [filter, filterActivities] = useState({
    email,
  });
  const [activityId, activityById] = useState(null);
  const fallback = [];
  const {
    data = fallback,
    isLoading,
    isError,
    error,
  } = useQuery(["activity", filter, activityId], async () =>
    getActivities(activityId, filter)
  );

  return {
    data,
    isLoading,
    isError,
    error,
    filter,
    filterActivities,
    activityById,
  };
}

export const fetchActivity = (activityId) => {
  return axios.get(`/activity-groups/${activityId}`).then((res) => res.data);
};

export function useDetailActivity(activityId) {
  return useQuery({
    queryKey: activityId && ["activity", activityId],
    queryFn: (key) => fetchActivity(activityId),
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    enabled: !!activityId,
  });
}
