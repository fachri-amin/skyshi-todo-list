import { useState } from "react";
import { useQuery } from "react-query";
import axios from "../../services/axios";

async function getTodos(id, filter) {
  const { data } = await axios.get(`/todo-items`, {
    params: { ...filter },
  });

  return data;
}

export function useTodo(activityGroupId) {
  const [filter, filterTodos] = useState({
    activity_group_id: activityGroupId,
  });
  const [todoId, todoById] = useState(null);
  const fallback = [];
  const {
    data = fallback,
    isLoading,
    isError,
    error,
  } = useQuery(["todo", filter, todoId], async () => getTodos(todoId, filter));

  return {
    data,
    isLoading,
    isError,
    error,
    filter,
    filterTodos,
    todoById,
  };
}

export const fetchTodo = (todoId) => {
  return axios.get(`/todo-items/${todoId}`).then((res) => res.data);
};

export function useDetailTodo(todoId) {
  return useQuery({
    queryKey: todoId && ["todo", todoId],
    queryFn: (key) => fetchTodo(todoId),
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    enabled: !!todoId,
  });
}
