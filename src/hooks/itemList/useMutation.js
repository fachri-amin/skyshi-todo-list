import axios from "../../services/axios";
import { useMutation, useQueryClient } from "react-query";

export function useAddTodo(options = {}) {
  return useMutation((values) =>
    axios.post("/todo-items", values).then((res) => res.data)
  );
}
export function useEditTodo(options = {}) {
  const queryClient = useQueryClient();

  return useMutation(
    (formData) =>
      axios
        .patch(`/todo-items/${formData.id}`, formData)
        .then((res) => res.data),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("todo");
        queryClient.removeQueries("todo");
      },
    }
  );
}

export function useDeleteTodo(options = {}) {
  const queryClient = useQueryClient();

  return useMutation(
    (id) => axios.delete(`/todo-items/${id}`).then((res) => res.data),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("todo");
        queryClient.removeQueries("todo");
      },
    }
  );
}
