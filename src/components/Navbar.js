import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import NavbarBootstrap from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";

const Navbar = () => {
  return (
    <NavbarBootstrap data-cy="navbar" className="my-bg-primary" expand="lg">
      <Container className="my-navbar">
        <NavbarBootstrap.Brand
          className="my-text-white navbar-brand"
          href="#home"
        >
          To do List App
        </NavbarBootstrap.Brand>
      </Container>
    </NavbarBootstrap>
  );
};

export default Navbar;
