import ModalBootstrap from "react-bootstrap/Modal";
import Button from "./Button";
import DeleteImage from "../assets/img/modal-delete-icon.png";

function ModalConfirmation({
  show,
  onReject,
  onApprove,
  subject,
  type = "activity",
}) {
  return (
    <ModalBootstrap data-cy="modal-confirmation" show={show} centered>
      <ModalBootstrap.Body className="text-center mt-2">
        <img src={DeleteImage} alt="" />
        <p className="f-18 m-1 mt-3">Apakah anda ingin menghapus {type}</p>
        <p className="f-18 fw-700">"{subject}"?</p>
        <div className="d-flex justify-content-center m-3 mt-4">
          <div class="mx-3">
            <Button
              data-cy="modal-delete-cancel-button"
              onClick={onReject}
              type="secondary"
              text={"Batal"}
            />
          </div>
          <div class="mx-3">
            <Button
              data-cy="modal-delete-confirm-button"
              onClick={onApprove}
              type="danger"
              text={"Hapus"}
            />
          </div>
        </div>
      </ModalBootstrap.Body>
    </ModalBootstrap>
  );
}

export default ModalConfirmation;
