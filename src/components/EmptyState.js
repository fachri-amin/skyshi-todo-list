import React from "react";
import ActivityEmptyStateImage from "../assets/img/activity-empty-state.png";
import TodoEmptyStateImage from "../assets/img/todo-empty-state.png";

const EmptyState = ({ type = "activity" }) => {
  switch (type) {
    case "activity":
      return (
        <div className="empty-state-container" data-cy="activity-empty-state">
          <img src={ActivityEmptyStateImage} alt="" />
        </div>
      );
    case "todo":
      return (
        <div className="empty-state-container" data-cy="activity-empty-state">
          <img src={TodoEmptyStateImage} alt="" />
        </div>
      );
    default:
      return (
        <div className="empty-state-container" data-cy="activity-empty-state">
          <img src={ActivityEmptyStateImage} alt="" />
        </div>
      );
  }
};

export default EmptyState;
