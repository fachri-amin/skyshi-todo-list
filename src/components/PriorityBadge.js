import React from "react";

const PriorityBadge = ({ color }) => {
  return (
    <div
      data-cy="priority-badge"
      className="priority-badge me-3"
      style={{ backgroundColor: color }}
    ></div>
  );
};

export default PriorityBadge;
