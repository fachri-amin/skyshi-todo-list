import React from "react";
import Dropdown from "react-bootstrap/Dropdown";
import IconSort from "../assets/img/todo-sort-button.png";
import IconLatest from "../assets/img/sort-latest.png";
import IconOldest from "../assets/img/sort-oldest.png";
import IconAz from "../assets/img/sort-az.png";
import IconZa from "../assets/img/sort-za.png";
import IconUnfinished from "../assets/img/sort-unfinished.png";
import IconChecklist from "../assets/img/checklist.png";

const sortOptions = [
  {
    icon: IconLatest,
    text: "Terbaru",
    dataCy: "todo-sort-button",
  },
  {
    icon: IconOldest,
    text: "Terlama",
    dataCy: "sort-selection",
  },
  {
    icon: IconAz,
    text: "A-Z",
    dataCy: "todo-sort-button",
  },
  {
    icon: IconZa,
    text: "Z-A",
    dataCy: "todo-sort-button",
  },
  {
    icon: IconUnfinished,
    text: "Belum Selesai",
    dataCy: "todo-sort-button",
  },
];

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <div
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
  </div>
));

const SortDropdown = ({ onClickItem = () => {}, selected = "" }) => {
  return (
    <Dropdown data-cy="sort-dropdown">
      <Dropdown.Toggle as={CustomToggle}>
        <img className="cursor-pointer ms-3 me-4" src={IconSort} alt="" />
      </Dropdown.Toggle>

      <Dropdown.Menu className="sort-dropdown-container">
        {sortOptions.map((item, index) => (
          <>
            <Dropdown.Item data-cy={item.dataCy}>
              <div className="sort-dropdown-item">
                <div className="sort-dropdown-item">
                  <img src={item.icon} alt="" />
                  <p className="m-0 ms-3">{item.text}</p>
                </div>
                {selected === item.text && <img src={IconChecklist} alt="" />}
              </div>
            </Dropdown.Item>
            {index !== sortOptions.length - 1 && <Dropdown.Divider />}
          </>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default SortDropdown;
