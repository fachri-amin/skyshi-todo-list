import React from "react";
import Dropdown from "react-bootstrap/Dropdown";
import PriorityBadge from "./PriorityBadge";
import ChevronDownIcon from "../assets/img/chevron-down.png";
import ChevronUpIcon from "../assets/img/chevron-up.png";
import IconChecklist from "../assets/img/checklist.png";

const priorityOptions = [
  {
    text: "Very High",
    value: "very-high",
    color: "#ED4C5C",
  },
  {
    text: "High",
    value: "high",
    color: "#F8A541",
  },
  {
    text: "Medium",
    value: "normal",
    color: "#00A790",
  },
  {
    text: "Low",
    value: "low",
    color: "#428BC1",
  },
  {
    text: "Very Low",
    value: "very-low",
    color: "#8942C1",
  },
];

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  React.useImperativeHandle(ref, () => ({
    setToggle() {
      setIsMenuOpen(!isMenuOpen);
    },
  }));
  return (
    <div
      className="custom-toggle-priority-select"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        setIsMenuOpen(!isMenuOpen);
        onClick(e);
      }}
    >
      {children}
      {isMenuOpen && <img src={ChevronUpIcon} alt="" />}
      {!isMenuOpen && <img src={ChevronDownIcon} alt="" />}
    </div>
  );
});

function PrioritySelect({ onSelect }) {
  const [selected, setSelected] = React.useState("");
  const [selectedBadgeColor, setSelectedBadgeColor] = React.useState("");
  const toggleRef = React.useRef(null);

  const handleSelect = (option) => {
    setSelected(option.text);
    setSelectedBadgeColor(option.color);
    onSelect(option.value);
    toggleRef.current.setToggle();
  };

  return (
    <Dropdown data-cy="priority-dropdown">
      <Dropdown.Toggle as={CustomToggle} id="dropdown-basic" ref={toggleRef}>
        {selected && selectedBadgeColor && (
          <div class="priority-select-option">
            <PriorityBadge color={selectedBadgeColor} />
            {selected}
          </div>
        )}
        {!selected && "Pilih priority"}
      </Dropdown.Toggle>

      <Dropdown.Menu>
        {priorityOptions.map((item) => (
          <Dropdown.Item href="#/action-1">
            <div
              onClick={() => handleSelect(item)}
              className="priority-select-option-container"
            >
              <div
                onClick={() => handleSelect(item)}
                className="priority-select-option"
              >
                <PriorityBadge color={item.color} />
                <p className="m-0">{item.text}</p>
              </div>
              {item.text === selected && (
                <img className="text-right" src={IconChecklist} alt="" />
              )}
            </div>
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default PrioritySelect;
