import React from "react";
import ModalBootstrap from "react-bootstrap/Modal";
import AlertIcon from "../assets/img/alert-icon.png";

function Snackbar({ show, type = "Activity" }) {
  return (
    <ModalBootstrap data-cy="modal-information" show={show}>
      <ModalBootstrap.Body className="px-4">
        <img className="snackbar-icon" src={AlertIcon} alt="" />
        <p className="f-18 ms-3 snackbar-text m-0">{type} berhasil dihapus</p>
      </ModalBootstrap.Body>
    </ModalBootstrap>
  );
}

export default Snackbar;
