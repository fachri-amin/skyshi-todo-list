import React from "react";

const Button = ({
  isSubmit,
  isAdd,
  text,
  disabled,
  type,
  onClick = () => {},
}) => {
  switch (type) {
    case "primary":
      return (
        <div
          data-cy="button"
          className={`my-button my-button-primary`}
          onClick={onClick}
          type={isSubmit ? "submit" : "button"}
        >
          <p className="my-text-button my-text-white">{text}</p>
        </div>
      );
    case "primary-add":
      return (
        <div
          data-cy="activity-add-button"
          className={`my-button my-button-primary`}
          onClick={onClick}
        >
          <p className="my-text-button my-text-white">&#43;{text}</p>
        </div>
      );
    case "secondary":
      return (
        <div
          data-cy="button"
          className={`my-button my-button-secondary`}
          onClick={onClick}
        >
          <p className="my-text-button">{text}</p>
        </div>
      );
    case "danger":
      return (
        <div
          data-cy="button"
          className={`my-button my-button-danger`}
          onClick={onClick}
        >
          <p className="my-text-button my-text-white">{text}</p>
        </div>
      );
    default:
      return (
        <div
          data-cy="button"
          className={`my-button my-button-primary`}
          onClick={onClick}
        >
          <p className="my-text-button my-text-white">{text}</p>
        </div>
      );
  }
};

export default Button;
