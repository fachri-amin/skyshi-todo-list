import React from "react";
import Form from "react-bootstrap/Form";
import EditIcon from "../assets/img/todo-title-edit-button.png";

function useOutside(ref, callback = () => {}) {
  React.useEffect(() => {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        callback();
      }
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref, callback]);
}

function EditableTitle({ text, onBlur }) {
  const [editMode, setEditMode] = React.useState(false);
  const [textValue, setTextValue] = React.useState(text);
  const inputRef = React.useRef(null);
  useOutside(inputRef, () => {
    onBlur({ title: textValue });
    setEditMode(false);
  });
  return (
    <div data-cy="header-title" className="d-flex align-items-center">
      {!editMode && (
        <p data-cy="header-title" className="section-title me-3">
          {text}
        </p>
      )}
      {editMode && (
        <Form.Control
          ref={inputRef}
          className="section-title section-title-input"
          plaintext
          defaultValue={text}
          value={textValue}
          onChange={(e) => setTextValue(e.target.value)}
          autoFocus
        />
      )}
      <img
        className="cursor-pointer"
        onClick={() => setEditMode(!editMode)}
        src={EditIcon}
        alt=""
      />
    </div>
  );
}

export default EditableTitle;
