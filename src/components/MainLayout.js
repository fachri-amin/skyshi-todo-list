/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import Container from "react-bootstrap/Container";
import { useNavigate } from "react-router";
import Navbar from "./Navbar";

const MainLayout = ({ children, title = "title" }) => {
  return (
    <div data-cy="main-layout" className="">
      <Navbar />
      <Container className="main-container">{children}</Container>
    </div>
  );
};

export default MainLayout;
