import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import ModalBootstrap from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "./Button";
import { Col, Row } from "react-bootstrap";
import PrioritySelect from "./PriorityDropdown";

const SchemaValidation = Yup.object().shape({
  activity_group_id: Yup.string().required("activity_group_id wajib diisi"),
  title: Yup.string().required("title wajib diisi"),
  priority: Yup.string().required("priority wajib diisi"),
});

function ModalAddItem({ show, handleClose, activityGrupId, handleFormSubmit }) {
  const formikRef = React.useRef(null);

  return (
    <ModalBootstrap
      data-cy="modal-add-item"
      size="lg"
      show={show}
      onHide={handleClose}
      centered
    >
      <Formik
        initialValues={{
          activity_group_id: activityGrupId,
          title: "",
          priority: "",
        }}
        innerRef={formikRef}
        validationSchema={SchemaValidation}
        onSubmit={handleFormSubmit}
      >
        {({
          handleSubmit,
          handleBlur,
          handleChange,
          values,
          touched,
          errors,
          setFieldValue,
        }) => (
          <Form onSubmit={handleSubmit} data-cy="todo-add-button">
            <ModalBootstrap.Header closeButton>
              <ModalBootstrap.Title>Tambah List Item</ModalBootstrap.Title>
            </ModalBootstrap.Header>
            <ModalBootstrap.Body>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
                data-cy="modal-add-name-input"
              >
                <Form.Label className="my-input-label">
                  NAMA LIST ITEM
                </Form.Label>
                <Form.Control
                  size="lg"
                  placeholder="Tambahkan nama list item"
                  value={values?.title}
                  onBlur={handleBlur}
                  onChange={(e) => setFieldValue("title", e.target.value)}
                  isInvalid={errors?.title && touched?.title}
                />
              </Form.Group>
              <Row>
                <Col lg={3}>
                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlInput1"
                    data-cy="modal-add-priority-dropdown"
                  >
                    <Form.Label className="my-input-label">PRIORITY</Form.Label>
                    <PrioritySelect
                      onSelect={(value) => setFieldValue("priority", value)}
                    />
                  </Form.Group>
                </Col>
              </Row>
            </ModalBootstrap.Body>
            <ModalBootstrap.Footer>
              <Button
                data-cy="modal-add-save-button"
                onClick={handleSubmit}
                isSubmit={true}
                text={"Simpan"}
              />
            </ModalBootstrap.Footer>
          </Form>
        )}
      </Formik>
    </ModalBootstrap>
  );
}

export default ModalAddItem;
