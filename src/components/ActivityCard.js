import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import DeleteIcon from "../assets/img/activity-item-delete-button.png";

const ActivityCard = ({
  onClick = () => {},
  title,
  date,
  onClickDelete = () => {},
}) => {
  return (
    <Card data-cy="activity-card" className="activity-card">
      <p
        onClick={onClick}
        data-cy="activity-title"
        className="activity-card-title"
      >
        {title}
      </p>
      <div onClick={onClick} class="activity-card-gap"></div>
      <Row className="justify-content-space-between align-items-center">
        <Col lg={9}>
          <p className="activity-card-date">{date}</p>
        </Col>
        <Col lg={1}>
          <img
            onClick={onClickDelete}
            className="activity-card-delete"
            src={DeleteIcon}
            alt=""
          />
        </Col>
      </Row>
    </Card>
  );
};

export default ActivityCard;
