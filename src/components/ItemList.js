import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import DeleteIcon from "../assets/img/activity-item-delete-button.png";
import PriorityBadge from "./PriorityBadge";
import IconPencil from "../assets/img/todo-item-edit-button.png";

const ItemList = ({
  onClick = () => {},
  title,
  checked,
  badgeColor,
  onCheck,
  onClickDelete = () => {},
}) => {
  return (
    <Card data-cy="item-list" className="item-list-card" onClick={onClick}>
      <Row className="justify-content-space-between align-items-center">
        <Col lg={11}>
          <div className="item-list-data-container">
            <Form.Check.Input
              data-cy="todo-item-checkbox"
              checked={checked}
              type={"checkbox"}
              className="me-3"
              onChange={onCheck}
            />
            <PriorityBadge color={badgeColor} className="me-3" />
            {checked === 1 && (
              <p className="item-list-card-title me-3">
                <s>{title}</s>
              </p>
            )}
            {!checked && <p className="item-list-card-title me-3">{title}</p>}
            <img className="item-list-card-edit" src={IconPencil} alt="" />
          </div>
        </Col>
        <Col lg={1}>
          <img
            data-cy="todo-item-delete-button"
            onClick={onClickDelete}
            className="item-list-card-delete"
            src={DeleteIcon}
            alt=""
          />
        </Col>
      </Row>
    </Card>
  );
};

export default ItemList;
